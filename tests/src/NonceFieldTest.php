<?php
/*
 *
 * (c) Tanseer UL Hassan
 *
 */
namespace Tanseercena\OopNonce\Tests;

use Brain\Monkey\Functions;
use Brain\Monkey\Filters;
use Tanseercena\OopNonce\Nonce;
use Tanseercena\OopNonce\NonceField;

/**
 * @author  Tanseer UL Hassan <tanseercena@gmail.com>
 * @package Tanseercena\OopNonce
 */
class NonceFieldTest extends TestCase
{
  /**
   * mock some wp functions
   */
    public function setUp()
    {
        Functions\when('wp_create_nonce')->alias('sha1');  // make it sha1

        Functions\when('wp_unslash')->alias(function ($value) {
            return is_string($value) ? stripslashes($value) : $value;
        });

        Functions\when('esc_attr')->alias(function ($url) {
            return filter_var($url, FILTER_SANITIZE_STRING);
        });
      //Set REQUEST_URI because we are running from cli not browser
        $_SERVER['REQUEST_URI'] = 'http://tanseer.com/xyz/';

        Functions\expect('sanitize_key')->andReturnUsing(function ($key) {
            return $key;
        });
        Functions\expect('esc_html')->andReturnUsing(function ($value) {
            return $value;
        });

        parent::setUp();
    }

  /**
   * Testing generate method
   */
    public function testGenerateWithoutDisplayAndReferer()
    {
        $action = 'test-action';
        $string_nonce = new Nonce($action);

        $nonce_field = new NonceField($action, false, false);

        $nonce = $nonce_field->getNonce();

        $test_nonce = sprintf(
            '<input type="hidden" name="%s" value="%s" />',
            '_wpnonce',
            esc_attr($string_nonce->getNonce())
        );

        $this->assertSame($nonce, $test_nonce);
    }

    public function testGenerateWithDisplayAndWithoutReferer()
    {
        $action = 'test-action';
        $string_nonce = new Nonce($action);

        $nonce_field = new NonceField($action, false, true);

        $nonce = $nonce_field->getNonce();

        $test_nonce = sprintf(
            '<input type="hidden" name="%s" value="%s" />',
            '_wpnonce',
            esc_attr($string_nonce->getNonce())
        );

        $this->expectOutputString($test_nonce);

        $this->assertSame($nonce, $test_nonce);
    }

    public function testGenerateWithDisplayAndReferer()
    {
        $action = 'test-action';
        $string_nonce = new Nonce($action);

        $nonce_field = new NonceField($action, true, true);

        $nonce = $nonce_field->getNonce();

        $test_nonce = sprintf(
            '<input type="hidden" name="%s" value="%s" />',
            '_wpnonce',
            esc_attr($string_nonce->getNonce())
        );
        $test_nonce_referer = sprintf(
            '<input type="hidden" name="%s" value="%s" />',
            '_wp_http_referer',
            esc_attr(wp_unslash($_SERVER['REQUEST_URI']))
        );

        $this->expectOutputString($test_nonce.$test_nonce_referer);

        $this->assertSame($nonce, $test_nonce.$test_nonce_referer);
    }
}
