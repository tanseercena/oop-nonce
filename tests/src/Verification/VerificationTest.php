<?php
/*
 *
 * (c) Tanseer UL Hassan
 *
 */
namespace Tanseercena\OopNonce\Tests;

use Brain\Monkey\Functions;
use Brain\Monkey\Filters;
use Tanseercena\OopNonce\Nonce;
use Tanseercena\OopNonce\NonceUrl;
use Tanseercena\OopNonce\NonceField;
use Tanseercena\OopNonce\Verification\Verification;

/**
 * @author  Tanseer UL Hassan <tanseercena@gmail.com>
 * @package Tanseercena\OopNonce
 */
class VerificationTest extends TestCase
{
  /**
   * mock some wp functions
   */
    public function setUp()
    {
        Functions\when('wp_create_nonce')->alias('sha1');  // make it sha1
        Functions\expect('home_url')->withNoArgs()->andReturn('http://tanseer.com/oopnonce');

        Functions\when('wp_unslash')->alias(function ($value) {
            return is_string($value) ? stripslashes($value) : $value;
        });

        Functions\when('esc_attr')->alias(function ($url) {
            return filter_var($url, FILTER_SANITIZE_STRING);
        });
      //Set REQUEST_URI because we are running from cli not browser
        $_SERVER['REQUEST_URI'] = 'http://tanseer.com/xyz/';

        Functions\expect('add_query_arg')
          ->with(\Mockery::type('string'), \Mockery::type('string'), \Mockery::type('string'))
          ->andReturnUsing(function ($key, $value, $url) {
              $glue = strpos($url, '?') ? '&' : '?';
              return "{$url}{$glue}{$key}={$value}";
          });
        Functions\expect('add_query_arg')->withNoArgs()->andReturn('/oopnonce/testing');
        Functions\when('esc_url_raw')->alias(function ($url) {
            return filter_var($url, FILTER_SANITIZE_URL);
        });
        Functions\expect('wp_verify_nonce')->andReturnUsing(function ($nonce, $action) {
            return sha1($action) === $nonce;
        });
        Functions\when('wp_unslash')->alias(function ($value) {
            return is_string($value) ? stripslashes($value) : $value;
        });
        Functions\when('sanitize_text_field')->alias(function ($value) {
            return $value;
        });
        Functions\expect('sanitize_key')->andReturnUsing(function ($key) {
            return $key;
        });
        Functions\expect('esc_html')->andReturnUsing(function ($value) {
            return $value;
        });

        parent::setUp();
    }

    public function testVerification()
    {
        $action = 'test-action';
        $nonce = new Nonce($action);

        $verification = new Verification($nonce);
        $check = $verification->verify();

        $this->assertTrue($check);
    }

    public function testNonceFieldVerification()
    {
        $action = 'test-action';
        $name = "_wpnonce";

        $nonce_field = new NonceField($action);

      // Build $_REQUEST
        $_REQUEST[$name] = $nonce_field->getGeneratedNonce();

        $verification = new Verification($nonce_field);
        $check = $verification->verify();

        $this->assertTrue($check);
    }

    public function testFailNonceFieldVerification()
    {
        $action = 'test-action';
        $name = "_wpnonce";

        $nonce_field = new NonceField($action);

      // Build $_REQUEST
        $_REQUEST[$name] = $nonce_field->getGeneratedNonce()."_Fail";

        $verification = new Verification($nonce_field);
        $check = $verification->verify();

        $this->assertFalse($check);
    }

    public function testNonceUrlVerification()
    {
        $action = 'test-action';
        $url = "http://tanseer.com/oopnonce";
        $name = "_wpnonce_url";

        $nonce_url = new NonceUrl($action, $url, $name);

      // Build $_REQUEST
        $_REQUEST[$name] = $nonce_url->getGeneratedNonce();

        $verification = new Verification($nonce_url);
        $check = $verification->verify();

        $this->assertTrue($check);
    }

    public function testFailNonceUrlVerification()
    {
        $action = 'test-action';
        $url = "http://tanseer.com/oopnonce";
        $name = "_wpnonce_url_xyz";

        $nonce_url = new NonceUrl($action, $url, $name);

      // Build $_REQUEST
        $_REQUEST[$name] = $nonce_url->getGeneratedNonce().'_Fail';

        $verification = new Verification($nonce_url);
        $check = $verification->verify();

        $this->assertFalse($check);

      // Build $_REQUEST
        $_REQUEST[$name.'_Fail'] = $nonce_url->getGeneratedNonce();

        $verification = new Verification($nonce_url);
        $check = $verification->verify();

        $this->assertFalse($check);
    }
}
