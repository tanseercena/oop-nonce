<?php
/*
 *
 * (c) Tanseer UL Hassan
 *
 */
namespace Tanseercena\OopNonce\Tests;

use Brain\Monkey\Functions;
use Brain\Monkey\Filters;
use Tanseercena\OopNonce\NonceFactory;
use Tanseercena\OopNonce\Nonce;
use Tanseercena\OopNonce\NonceUrl;
use Tanseercena\OopNonce\NonceField;

/**
 * @author  Tanseer UL Hassan <tanseercena@gmail.com>
 * @package Tanseercena\OopNonce
 */
class NonceFactoryTest extends TestCase
{
  /**
   * mock some wp functions
   */
    public function setUp()
    {
        Functions\when('wp_create_nonce')->alias('sha1');  // make it sha1
        Functions\expect('home_url')->withNoArgs()->andReturn('http://tanseer.com/oopnonce');

        Functions\when('wp_unslash')->alias(function ($value) {
            return is_string($value) ? stripslashes($value) : $value;
        });

        Functions\when('esc_attr')->alias(function ($url) {
            return filter_var($url, FILTER_SANITIZE_STRING);
        });
      //Set REQUEST_URI because we are running from cli not browser
        $_SERVER['REQUEST_URI'] = 'http://tanseer.com/xyz/';

        Functions\expect('add_query_arg')
          ->with(\Mockery::type('string'), \Mockery::type('string'), \Mockery::type('string'))
          ->andReturnUsing(function ($key, $value, $url) {
              $glue = strpos($url, '?') ? '&' : '?';
              return "{$url}{$glue}{$key}={$value}";
          });
        Functions\expect('add_query_arg')->withNoArgs()->andReturn('/oopnonce/testing');
        Functions\when('esc_url_raw')->alias(function ($url) {
            return filter_var($url, FILTER_SANITIZE_URL);
        });
        Functions\expect('wp_verify_nonce')->andReturnUsing(function ($nonce, $action) {
            return sha1($action) === $nonce;
        });
        Functions\expect('sanitize_key')->andReturnUsing(function ($key) {
            return $key;
        });
        Functions\expect('esc_html')->andReturnUsing(function ($value) {
            return $value;
        });

        parent::setUp();
    }

    public function testNonceFactoryForNonceObject()
    {
        $nonce = NonceFactory::create(Nonce::class, 'test-action');

        $this->assertInstanceOf(Nonce::class, $nonce);
    }

    public function testFailNonceFactoryForNonceObject()
    {
        $nonce = NonceFactory::create(Nonce::class, 'test-action');

        $this->assertNotInstanceOf(NonceUrl::class, $nonce);
    }

    public function testNonceFactoryForNonceObjectWithStringArg()
    {
        $nonce = NonceFactory::create('nonce_field', 'test-action');

        $this->assertInstanceOf(NonceField::class, $nonce);
    }

    public function testFailNonceFactoryForNonceObjectWithStringArg()
    {
        $nonce = NonceFactory::create('nonce_field', 'test-action');

        $this->assertNotInstanceOf(Nonce::class, $nonce);
    }
}
