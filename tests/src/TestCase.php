<?php declare( strict_types=1 );
/*
 *
 * (c) Tanseer UL Hassan
 *
 */
namespace Tanseercena\OopNonce\Tests;

// Brain Monkey tool is used for wordpress function testing etc
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Brain\Monkey;

/**
* @author  Tanseer UL Hassan <tanseercena@gmail.com>
* @package Tanseercena\OopNonce
*/
class TestCase extends \PHPUnit\Framework\TestCase
{
    protected function setUp()
    {
        parent::setUp();
        Monkey\setUp();
    }

    protected function tearDown()
    {
        Monkey\tearDown();
        parent::tearDown();
    }
}
