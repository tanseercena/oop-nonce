<?php
/*
 *
 * (c) Tanseer UL Hassan
 *
 */
namespace Tanseercena\OopNonce\Tests;

use Brain\Monkey\Functions;
use Brain\Monkey\Filters;
use Tanseercena\OopNonce\Nonce;
use Tanseercena\OopNonce\NonceUrl;

/**
 * @author  Tanseer UL Hassan <tanseercena@gmail.com>
 * @package Tanseercena\OopNonce
 */
class NonceUrlTest extends TestCase
{
  /**
   * mock functions which is used by NonceString and NonceUrl
   */
    public function setUp()
    {
        Functions\when('wp_create_nonce')->alias('sha1');  // make it sha1
        Functions\expect('home_url')->withNoArgs()->andReturn('http://tanseer.com/oopnonce');
      //Mock home_url funciton if given arguments
        Functions\expect('add_query_arg')
          ->with(\Mockery::type('string'), \Mockery::type('string'), \Mockery::type('string'))
          ->andReturnUsing(function ($key, $value, $url) {
              $glue = strpos($url, '?') ? '&' : '?';
              return "{$url}{$glue}{$key}={$value}";
          });
        Functions\expect('add_query_arg')->withNoArgs()->andReturn('/oopnonce/testing');
        Functions\when('esc_url_raw')->alias(function ($url) {
            return filter_var($url, FILTER_SANITIZE_URL);
        });

        parent::setUp();
    }

  /**
   * Testing generate method
   */
    public function testGenerate()
    {
        $action = 'test-action';
        $string_nonce = new Nonce($action);
        $url = "http://tanseer.com/oopnonce";

        $nonce_url = new NonceUrl($action, $url);

        $nonce = $nonce_url->getNonce();
        $glue = strpos($url, '?') ? '&' : '?';
        $test_nonce = $url.$glue."_wpnonce=".$string_nonce->getNonce();

        $this->assertSame($nonce, $test_nonce);
    }
}
