<?php
/*
 *
 * (c) Tanseer UL Hassan
 *
 */
namespace Tanseercena\OopNonce\Tests;

use Brain\Monkey\Functions;
use Brain\Monkey\Filters;
use Tanseercena\OopNonce\Nonce;

/**
 * @author  Tanseer UL Hassan <tanseercena@gmail.com>
 * @package Tanseercena\OopNonce
 */
class NonceTest extends TestCase
{
  /**
   * Testing generate method
   */
    public function testGenerate()
    {
        $hash = sha1('test-action');

        Functions\expect('wp_create_nonce')->once()->with('test-action')->andReturn(sha1($hash));

        $string_nonce = new Nonce('test-action');
        $nonce = $string_nonce->getNonce();

        $this->assertSame($nonce, sha1($hash));
    }

  /**
   * Test tickCallable method for seconds
   */
    public function testTickCallable()
    {
        $hash = sha1('test-action');
        Functions\expect('wp_create_nonce')->once()->with('test-action')->andReturn(sha1($hash));

        Filters\expectAdded('nonce_life')
        ->once()
        ->with(\Mockery::type('Closure'))
        ->whenHappen(function (\Closure $closure) {
            $this->assertSame(150, $closure());
        });

        $string_nonce = new Nonce('test-action', 150);
        $nonce = $string_nonce->getNonce();
    }

  /**
   * Test getAction method
   */
    public function testGetAction()
    {
        $action = 'test-action';
        $hash = sha1($action);

        Functions\expect('wp_create_nonce')->once()->with('test-action')->andReturn(sha1($hash));

        $string_nonce = new Nonce('test-action');

        $this->assertSame($action, $string_nonce->getAction());
    }
}
