<?php declare(strict_types=1);
/*
 *
 * (c) Tanseer UL Hassan
 *
 */
namespace Tanseercena\OopNonce;

use Tanseercena\OopNonce\Verification\VerificationInterface;

/**
 * Generate Nonce as string and can be used in different ways by user.
 *
 * @author  Tanseer UL Hassan <tanseercena@gmail.com>
 * @package Tanseercena\OopNonce
 */
class Nonce implements
    NonceInterface,
    VerificationInterface
{
    /**
     * Action Name
     * @var string
     */
    private $action;
    /**
     * Lifespan in seconds
     * @var int
     */
    private $tick;
    /**
     * Generated Nonce
     * @var string
     */
    private $nonce;

    /**
     * @param string  $action
     * @param integer $tick
     * @param string  $nonce_name
     */
    public function __construct(string $action, int $tick = 86400)
    {
        $this->action = $action;
        $this->tick = $tick;
        $this->nonce = '';
        //Generate Nonce
        $this->generate();
    }

    /**
     * Returns nonce action
     * @return string
     */
    public function getAction() : string
    {
        return $this->action;
    }

    public function verify() : bool
    {
        add_filter('nonce_life', $this->tickCallable());
        $check = wp_verify_nonce($this->nonce, $this->action);
        remove_filter('nonce_life', $this->tickCallable());

        return (bool) $check;
    }

    /**
     * Generate Nonce
     * @return void
     */
    public function generate()
    {
        add_filter('nonce_life', $this->tickCallable());
        $this->nonce = wp_create_nonce($this->action);
        remove_filter('nonce_life', $this->tickCallable());
    }

    /**
     * Tick lifespan Callable
     * @return \Closure
     */
    private function tickCallable()
    {
        return function () {
            return $this->tick;
        };
    }

    /**
     * Return nonce as string
     * @return string
     */
    public function getNonce() : string
    {
        return $this->nonce;
    }

    public function setNonce(string $nonce)
    {
        $this->nonce = $nonce;
    }
}
