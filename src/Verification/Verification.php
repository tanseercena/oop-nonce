<?php declare(strict_types=1);
/*
 *
 * (c) Tanseer UL Hassan
 *
 */
namespace Tanseercena\OopNonce\Verification;

/**
 * This is main verificaiton strategy class
 *
 * @author  Tanseer UL Hassan <tanseercena@gmail.com>
 * @package Tanseercena\OopNonce
 */
final class Verification
{
    /**
     * Nonce which will be verify
     * @var VerificationInterface
     */
    private $nonce;

    /**
     *
     * @param VerificationInterface $nonce
     */
    public function __construct(VerificationInterface $nonce)
    {
        $this->nonce = $nonce;
    }

    /**
     * Verify given nonce
     * @return bool
     */
    public function verify() : bool
    {
        return $this->nonce->verify();
    }
}
