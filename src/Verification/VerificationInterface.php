<?php declare(strict_types=1);
/*
 *
 * (c) Tanseer UL Hassan
 *
 */
namespace Tanseercena\OopNonce\Verification;

/**
 * This Interface used for Verification of Nonce
 * Verification process is done through Strategy Design Pattern
 *
 * @author  Tanseer UL Hassan <tanseercena@gmail.com>
 * @package Tanseercena\OopNonce
 */
interface VerificationInterface
{
  /**
   *
   * Verify Nonce
   *
   * @return bool
   */
    public function verify() : bool;
}
