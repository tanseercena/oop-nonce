<?php declare(strict_types=1);
/*
 *
 * (c) Tanseer UL Hassan
 *
 */
namespace Tanseercena\OopNonce;

/**
 * This Interface is for all nonce implementations.
 *
 * @author  Tanseer UL Hassan <tanseercena@gmail.com>
 * @package Tanseercena\OopNonce
 */
interface NonceInterface
{
    /**
     * Returns nonce action.
     *
     * @return string
     */
    public function getAction() : string;

    /**
     * Generate Nonce
     * @return void
     */
    public function generate();

    public function getNonce() : string;
}
