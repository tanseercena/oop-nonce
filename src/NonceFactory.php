<?php declare(strict_types=1);
/*
 *
 * (c) Tanseer UL Hassan
 *
 */
namespace Tanseercena\OopNonce;

use Tanseercena\OopNonce\Nonce;
use Tanseercena\OopNonce\NonceUrl;
use Tanseercena\OopNonce\NonceField;

/**
  * Factory for Nonce object creations
  *
  * @author  Tanseer UL Hassan <tanseercena@gmail.com>
  * @package Tanseercena\OopNonce
  */
class NonceFactory
{

    public static function create($nonceClass, string $action, array $options = [])
    {
        if ('nonce' == $nonceClass || 'Nonce' == join('', array_slice(explode('\\', $nonceClass), -1))) {
            return self::createNonceObject($action, $options);
        } elseif ('nonce_url' == $nonceClass || 'NonceUrl' == join('', array_slice(explode('\\', $nonceClass), -1))) {
            return self::createNonceUrlObject($action, $options);
        } elseif ('nonce_field' == $nonceClass || 'NonceField' == join('', array_slice(explode('\\', $nonceClass), -1))) {
            return self::createNonceFieldObject($action, $options);
        }

        return null;
    }

    private function createNonceObject(string $action, array $options = [])
    {
        $tick = isset($options['tick']) ? $options['tick'] : 86400;
        return new Nonce($action, $tick);
    }

    private function createNonceUrlObject(string $action, array $options = [])
    {
        $action_url = isset($options['action_url']) ? $options['action_url'] : null;
        $nonce_name = isset($options['nonce_name']) ? $options['nonce_name'] : '_wpnonce';
        return new NonceUrl($action, $action_url, $nonce_name);
    }

    private function createNonceFieldObject(string $action, array $options = [])
    {
        $referer = isset($options['referer']) ? $options['referer'] : true;
        $display = isset($options['display']) ? $options['display'] : true;
        $nonce_name = isset($options['nonce_name']) ? $options['nonce_name'] : '_wpnonce';
        return new NonceField($action, $referer, $display, $nonce_name);
    }
}
