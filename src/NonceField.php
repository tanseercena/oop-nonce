<?php declare(strict_types=1);
/*
 *
 * (c) Tanseer UL Hassan
 *
 */
namespace Tanseercena\OopNonce;

use Tanseercena\OopNonce\Verification\VerificationInterface;

/**
 * Generate URL Nonce
 *
 * @author  Tanseer UL Hassan <tanseercena@gmail.com>
 * @package Tanseercena\OopNonce
 */
class NonceField implements NonceInterface, VerificationInterface
{
  /**
   * Action Name
   * @var string
   */
    private $action;
  /**
   * Nonce name
   * @var string
   */
    private $nonceName;
  /**
   * Whether to show or not Referer field
   * @var bool
   */
    private $referer;
  /**
   * Print and/or return fields
   * @var bool
   */
    private $display;
  /**
   * Generated Nonce
   * @var string
   */
    private $nonce;
  /**
   * NonceString object
   * @var Nonce
   */
    private $nonceObj;

  /**
   * @param string $action
   * @param string $action_url
   * @param string $nonceName
   */
    public function __construct(
        string $action,
        bool $referer = true,
        bool $display = true,
        string $nonceName = '_wpnonce'
    ) {

        $this->action = $action;
        $this->referer = $referer;
        $this->display = $display;
        $this->nonceObj = new Nonce($action);
        $this->nonce_name = $nonceName;
      // Generate Nonce
        $this->generate();
    }

  /**
   * Returns nonce action
   * @return string
   */
    public function getAction() : string
    {
        return $this->action;
    }

    public function verify() : bool
    {
        if (isset($_REQUEST[$this->nonce_name])) {
            $nonceReceived = sanitize_text_field(wp_unslash($_REQUEST[$this->nonce_name]));
            $this->nonceObj->setNonce($nonceReceived);
            return $this->nonceObj->verify();
        }
        return false;
    }

  /**
   * Generate Nonce
   * @return void
   */
    public function generate()
    {
        $field = sprintf(
            '<input type="hidden" name="%s" value="%s" />',
            esc_attr($this->nonce_name),
            esc_attr($this->nonceObj->getNonce())
        );

        $refererField = "";
        if ($this->referer && isset($_SERVER['REQUEST_URI'])) {
            $refererField = sprintf(
                '<input type="hidden" name="%s" value="%s" />',
                '_wp_http_referer',
                esc_attr(wp_unslash(sanitize_key($_SERVER['REQUEST_URI'])))
            );
        }

        $this->nonce = $field.$refererField;

        if ($this->display) {
            echo esc_html($this->nonce);
        }
    }

  /**
   * Return generated nonce as string
   * @return string
   */
    public function getNonce() : string
    {
        return $this->nonce;
    }

  /**
   * Return Nonce Value
   * @return string
   */
    public function getGeneratedNonce() : string
    {
        return $this->nonceObj->getNonce();
    }
}
