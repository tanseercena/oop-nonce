<?php declare(strict_types=1);
/*
 *
 * (c) Tanseer UL Hassan
 *
 */
namespace Tanseercena\OopNonce;

use Tanseercena\OopNonce\Verification\VerificationInterface;

/**
 * Generate URL Nonce
 *
 * @author  Tanseer UL Hassan <tanseercena@gmail.com>
 * @package Tanseercena\OopNonce
 */
class NonceUrl implements NonceInterface, VerificationInterface
{
  /**
   * Action Name
   * @var string
   */
    private $action;
  /**
   * Nonce name
   * @var string
   */
    private $nonceName;
  /**
   * Action URL
   * @var string
   */
    private $actionUrl;
  /**
   * Generated Nonce
   * @var string
   */
    private $nonce;
  /**
   * NonceString object
   * @var Nonce
   */
    private $nonceObj;

  /**
   * @param string $action
   * @param string $actionUrl
   * @param string $nonceName
   */
    public function __construct(
        string $action,
        string $actionUrl = null,
        string $nonceName = '_wpnonce'
    ) {
      
        if (!is_string($actionUrl) || !$actionUrl) {
            $actionUrl = $this->generateUrl();
        }
        $this->action_url = str_replace('&amp;', '&', $actionUrl);
        $this->action = $action;
        $this->nonce_name = $nonceName;
        $this->nonceObj = new Nonce($action);
     // Generate Nonce
        $this->generate();
    }

  /**
   * Returns nonce action
   * @return string
   */
    public function getAction() : string
    {
        return $this->action;
    }

    public function verify() : bool
    {
        if (isset($_REQUEST[$this->nonce_name])) {
            $nonceReceived = sanitize_text_field(wp_unslash($_REQUEST[$this->nonce_name]));
            $this->nonceObj->setNonce($nonceReceived);
            return $this->nonceObj->verify();
        }
        return false;
    }

  /**
   * Generate Nonce
   * @return void
   */
    public function generate()
    {
        $this->nonce = esc_url_raw(
            add_query_arg($this->nonce_name, $this->nonceObj->getNonce(), $this->action_url)
        );
    }

  /**
   * Return nonce as string
   * @return string
   */
    public function getNonce() : string
    {
        return $this->nonce;
    }

  /**
   * Generate URL if url is not set or empty
   * @var string
   */
    private function generateUrl() : string
    {
        $homePath = trim(parse_url(home_url(), PHP_URL_PATH), '/');
        $currentUrlPath = trim(add_query_arg(), '/');
        if ($homePath && strpos($currentUrlPath, $homePath) === 0) {
            $currentUrlPath = substr($currentUrlPath, strlen($homePath));
        }
        return home_url(urldecode($currentUrlPath));
    }

  /**
   * Return Nonce Value
   * @return string
   */
    public function getGeneratedNonce() : string
    {
        return $this->nonceObj->getNonce();
    }
}
